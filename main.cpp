#include "Saver.h"

int main(void)
{
	double x;
	double y;
	Saver Q("in", 10);
	Q.Get("x", x);
	Q.Check("x", x);
	Q.Set("xy", 123.0);
	Q.Get("xy", x);
	printf("%lf\n", x);
	Q.Save("in");
	return 0;
}

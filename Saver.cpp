#include "Saver.h"


Saver::Saver(const char *FileName, uint32_t k)
{
	FILE *fp;
	int i;
	int n;
	this->FileName = new char[strlen(FileName) + 1];
	strcpy(this->FileName, FileName);
	if (FileName == NULL)
	{
		printf("error in FileName");
		throw -2;
	}
	fp = fopen(FileName, "r");
	if (fp == NULL)
	{
		printf("error in file");
		throw -1;
	}
	if (fscanf(fp, "%d", &n) == 0)
	{
		n = 0;
	}
	this->N = n + k;
	this->K = n;
	this->len = new int[n + k];
	this->VarName = new char*[n + k];
	this->val = new char*[n + k];
	this->type = new int[n + k];
	this->ptr = new void*[n + k];
	for (i = 0; i <= n - 1 + k; i++)
	{
		this->VarName[i] = new char[100];
		this->val[i] = new char[100];
		this->ptr[i] = NULL;
	}
	if (n >= 1)
	{
		for(i = 0; i <= n - 1; i++)
		{
			fscanf(fp, "%d %s %d", &(this->type[i]), this->VarName[i], &(this->len[i]));
		}
		fgetc(fp);
		for (i = 0; i <= n - 1; i++)
		{
			int j;
			for(j = 0; j <= this->len[i] - 1; j++)
			{
				this->val[i][j] = fgetc(fp);
			}
		}
	}
}

void Saver::Get(const char *Name, int &x)
{
	int i;
	int n = this->K;
	for (i = 0; i <= n - 1; i++)
	{
		if (strcmp(Name, this->VarName[i]) == 0)
		{
			memcpy(&x, this->val[i], sizeof(int));
			return;
		}
	}
}

void Saver::Save(const char *FileName)
{
	FILE *fp;
	int i;
	int n = this->K;
	if (FileName == NULL)
	{
		FileName = this->FileName;
	}
	fp = fopen(FileName, "w");
	fprintf(fp, "%d\n", n);
	for (i = 0; i <= n - 1; i++)
	{
		if (ptr[i] != NULL)
		{
			memcpy(this->val[i], this->ptr[i], this->len[i]);
		}
	}
	for (i = 0; i <= n - 1; i++)
	{
		fprintf(fp, "%d %s %d\n", this->type[i], this->VarName[i], this->len[i]);
	}
	for (i = 0; i <= n - 1; i++)
	{
		int j;
		for (j = 0; j <= this->len[i] - 1; j++)
		{
			fputc(this->val[i][j], fp);
		}
	}
}

void Saver::Check(const char *Name, int &x)
{
	int i;
	if (Name == NULL)
	{
		printf("Check:: bad name\n");
		return ;
	}
	for (i = 0; i <= this->K - 1; i++)
	{
		if (strcmp(Name, this->VarName[i]) == 0)
		{
			this->ptr[i] = &x;
			memcpy(this->val[i], &x, sizeof(int));
			this->type[i] = 1;
			this->len[i] = sizeof(int);
			return ;
		}
	}
	if (this->K == this->N)
	{
		printf("Check:: bad size\n");
		return; 
	}
	else
	{
		this->ptr[this->K] = &x;
		memcpy(this->val[this->K], &x, sizeof(int));
		strcpy(this->VarName[i], Name);
		this->len[i] = sizeof(int);
		this->type[i] = 1;
		this->K++;
	}
}

void Saver::Check(const char *Name, double &x)
{
	int i;
	if (Name == NULL)
	{
		printf("Check:: bad name\n");
		return ;
	}
	for (i = 0; i <= this->K - 1; i++)
	{
		if (strcmp(Name, this->VarName[i]) == 0)
		{
			this->ptr[i] = &x;
			memcpy(this->val[i], &x, sizeof(double));
			this->type[i] = 2;
			this->len[i] = sizeof(double);
			return ;
		}
	}
	if (this->K == this->N)
	{
		printf("Check:: bad size\n");
		return; 
	}
	else
	{
		this->ptr[this->K] = &x;
		memcpy(this->val[this->K], &x, sizeof(double));
		strcpy(this->VarName[i], Name);
		this->len[i] = sizeof(double);
		this->type[i] = 2;
		this->K++;
	}

}

void Saver::Get(const char *Name, double &x)
{
	int i;
	int n = this->K;
	for (i = 0; i <= n - 1; i++)
	{
		if (strcmp(Name, this->VarName[i]) == 0)
		{
			memcpy(&x, this->val[i], sizeof(double));
			return;
		}
	}
}

Saver::~Saver(void)
{
	int i, n, k;
	n = this->N;
	k = this->K;
	for (i = 0; i <= n - 1; i++)
	{
		if (this->VarName[i] != NULL)
		delete[] this->VarName[i];
		if (this->val[i] != NULL)
		delete[] this->val[i];
	}
	delete[] this->len;
	delete[] this->VarName;
	delete[] this->val;
	delete[] this->type;
	delete[] this->ptr;
	delete[] this->FileName;
}

int Saver::WhatType(const char *Name)
{
	int i;
	int n = this->K;
	for (i = 0; i <= n - 1; i++)
	{
		if (strcmp(Name, this->VarName[i]) == 0)
		{
			return this->type[i];
		}
	}
}
void Saver::Fill(void)
{
	int i;
	for (i = 0; i <= this->K - 1; i++)
	{
		if (this->ptr[i] != NULL)
		{
			memcpy(this->val[i], this->ptr[i], this->len[i]);
		}
	}
}

void Saver::Check(const char *Name, char **x)
{
	int i;
	if (Name == NULL)
	{
		printf("Check:: bad name\n");
		return ;
	}
	for (i = 0; i <= this->K - 1; i++)
	{
		if (strcmp(Name, this->VarName[i]) == 0)
		{
			this->ptr[i] = *x;
			memcpy(this->val[i], *x, strlen(*x) + 1);
			this->type[i] = 3;
			this->len[i] = strlen(*x) + 1;
			return ;
		}
	}
	if (this->K == this->N)
	{
		printf("Check:: bad size\n");
		return; 
	}
	else
	{
		this->ptr[this->K] = *x;
		memcpy(this->val[this->K], *x, strlen(*x) + 1);
		strcpy(this->VarName[i], Name);
		this->len[i] = strlen(*x) + 1;
		this->type[i] = 3;
		this->K++;
	}

}


void Saver::Get(const char *Name, char **x)
{
	int i;
	int n = this->K;
	if (*x != NULL)
	{
		delete[] *x;
	}
	for (i = 0; i <= n - 1; i++)
	{
		if (strcmp(Name, this->VarName[i]) == 0)
		{
			(*x) = new char[this->len[i]];
			memcpy((*x), this->val[i], this->len[i]);
			return;
		}
	}
}

void Saver::Set(const char *Name, int a)
{
	int i;
	int n = this->K;
	for (i = 0; i <= n - 1; i++)
	{
		if (strcmp(Name, this->VarName[i]) == 0)
		{
			memcpy(this->val[i], &a, sizeof(int));
			return;
		}
	}
	if (this->K == this->N)
	{
		printf("Check:: bad size\n");
		return; 
	}
	else
	{
		this->ptr[this->K] = NULL;
		memcpy(this->val[this->K], &a, sizeof(int));
		strcpy(this->VarName[i], Name);
		this->len[i] = sizeof(int);
		this->type[i] = 1;
		this->K++;
	}
}

void Saver::Set(const char *Name, double a)
{
	int i;
	int n = this->K;
	for (i = 0; i <= n - 1; i++)
	{
		if (strcmp(Name, this->VarName[i]) == 0)
		{
			memcpy(this->val[i], &a, sizeof(double));
			return;
		}
	}
	if (this->K == this->N)
	{
		printf("Check:: bad size\n");
		return; 
	}
	else
	{
		this->ptr[this->K] = NULL;
		memcpy(this->val[this->K], &a, sizeof(double));
		strcpy(this->VarName[i], Name);
		this->len[i] = sizeof(double);
		this->type[i] = 2;
		this->K++;
	}
}

void Saver::Set(const char *Name, const char *a)
{
	int i;
	int n = this->K;
	for (i = 0; i <= n - 1; i++)
	{
		if (strcmp(Name, this->VarName[i]) == 0)
		{
			memcpy(this->val[i], a, strlen(a) + 1);
			return;
		}
	}
	if (this->K == this->N)
	{
		printf("Check:: bad size\n");
		return; 
	}
	else
	{
		this->ptr[this->K] = NULL;
		memcpy(this->val[this->K], a, strlen(a) + 1);
		strcpy(this->VarName[i], Name);
		this->len[i] = strlen(a) + 1;
		this->type[i] = 3;
		this->K++;
	}
}

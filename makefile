all: prog
prog: main.o Saver.o
	g++ main.o Saver.o -g -o prog
main.o: main.cpp
	g++ main.cpp -c -g -o main.o
Saver.o: Saver.cpp Saver.h
	g++ Saver.cpp -c -g -o Saver.o
clean:
	rm *.o
	rm prog

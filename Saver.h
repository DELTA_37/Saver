#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"

class Saver
{
private:
	char *FileName; // файл с данными
	char **VarName; // имя переменной
	int *len; // количество байт
	char **val; // сами байты
	int *type; // type 1- int 2-double 3-char
	int N; // maximum
	int K; // current table
	void **ptr;
public:
	Saver(const char *FileName, uint32_t k); //конструктор
	~Saver(void);
	void Get(const char *Name, int &x); // вытащить значение переменной
	void Get(const char *Name, double &x);
	void Get(const char *Name, char **x);
	void Save(const char *FileName);
	void Check(const char *Name, int &x); // (новая) инициализация переменной 
	void Check(const char *Name, double &x);
	void Check(const char *Name, char **x);
	void Fill(void); // обновить все поля
	void Set(const char *Name, int a); //изменить значение в самой структуре, теряется, если переменная зависима, при перезаписи
	void Set(const char *Name, double a);
	void Set(const char *Name, const char *a);
	int WhatType(const char *Name);
};
